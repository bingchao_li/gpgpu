\section{Cache Architecture}
\label{implementation}

In our approach, the number of sets, associativity and total capacity of L1 cache are not changed. The baseline configuration of L1 cache we use in this paper is shown in TABLE \ref{baseline cache}. To make the cache accessed in fine granularity, we divide a 128-byte cache line into 4 32-byte logical chunks as illustrated in Fig. \ref{example_diagram} (i.e., 16 32-byte chunks per set in a 4-way set associative cache). Each chunk is equipped with a tag which is called chunk-tag in this paper. We store chunk-tags in the unused region of the shared memory. In this paper, we do not provide fine-grain cache management for requests of 64 bytes since these requests are much less than 32-byte requests. Requests of 64 bytes are processed in the same way of 128-byte requests.

\makeatletter
\def\hlinewd#1{%
  \noalign{\ifnum0=`}\fi\hrule \@height #1 \futurelet
   \reserved@a\@xhline}
\makeatother

\begin{table}
  \centering
  \begin{tabular}{!{\vrule width 1.5pt}c|c|c!{\vrule width 1.5pt}}
    \hlinewd{1.5pt}
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
     Cache capacity& 16KB & 48KB \\
    \hline
    Number of sets & 32 & 64 \\
    \hline
    Associativity & 4 & 6 \\
    \hline
    Cache line size & 128 bytes & 128 bytes \\
    \hlinewd{1.5pt}
  \end{tabular}
  \caption{Baseline cache configuration}\label{baseline cache}
\end{table}

\subsection{Chunk-tag Design}

We take the case of 16KB L1 cache as an example to illustrate our mechanism. The minimum request size is 32 bytes according to the segmentation mechanism we mentioned in Section \ref{request_size_ratio}. The Fermi architecture's shared memory is divided into 32 banks, which are organized such that successive 32-bit words are assigned to successive banks and the bandwidth is 32 bits per bank per cycle \cite{CUDA}. Considering the alignment in banks of the shared memory, we propose two types of chunk-tag widths, 16 and 32 bits.

Firstly, we discuss the design of 16-bit chunk-tag. In this mechanism, 16 bits are used to store chunk-tags. In modern GPUs, the total capacity of the main memory is usually more than 1GB. Thus, 16 bits are not sufficient to store all the necessary tag bits for each 32-byte chunk in address comparisons. To efficiently provide the full tag bits, we leverage the original tag bits for each 128-byte line of conventional caches as a common-tag. We assume that the full address space is comprised of 36 bits (64GB address space) as shown in Fig. \ref{chunk_tag}. For a conventional 16KB cache with 32 128-byte sets, the default length of $Set\_Index$ and $Byte\_Index$ are comprised of 5 and 7 bits, respectively. Then the remaining 24 bits are used as tags. In Fig. \ref{chunk_tag_16bit}, as each cache line contains 4 32-byte chunks, we borrow the upper 2 bits from the $Byte\_Index$ to comprise the lower 2 bits of the 16-bit chunk-tag. The upper 14 bits are obtained from the lower 14 bits of the 24-bit tag. Consequently, the common-tag is comprised of only the upper 10 bits of the 24-bit tag. In summary, a 128 bytes cache line has one 10-bit common-tag and four 16-bit chunk-tags. A common-tag of a cache line indicates an address space of $2^{16+7-2}$ bytes, namely 2MB. Therefore, no matter whether 32-byte data blocks are in contiguous address space or not, as long as they can find a cache line that has the same common-tag with them, they are qualified to be filled into any of the 4 32-byte chunks indexed by the 16-bit chunk-tags of this cache line.

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.5\textwidth]{figures/example_diagram.eps}\\
  \caption{L1 cache line is divided into 4 32-byte chunks associated with chunk-tags }\label{example_diagram}
\end{figure}

\begin{figure} \centering
\subfloat[16-bit chunk-tag] { \label{chunk_tag_16bit}
\includegraphics[width=0.49\textwidth]{figures/chunk_tag_16bit.eps}
}
\\
\subfloat[32-bit chunk-tag] { \label{chunk_tag_32bit}
\includegraphics[width=0.49\textwidth]{figures/chunk_tag_32bit.eps}
}
\caption{Context of tags stored in the shared memory}
\label{chunk_tag}
\end{figure}


One common-tag and 4 chunk-tags per cache line are all required during cache accesses. We can store the common-tags in the own tag array of L1 cache; 4 common-tags and 16 chunk-tags inside one set are read out simultaneously. Then the address of the request is compared with the all the 4 common-tags and 16 chunk-tags. For a 32-byte request, only when one common-tag and one of its corresponding 4 chunk-tags are both matched we can claim that a hit occurs. For a 128-byte request, a hit indicates that one common-tag and its four chunk-tags are all matched. For instance, a 32-byte request whose address is 0x008000002 needs to access L1 cache. Its common-tag and chunk-tag are 0x001 and 0x0002, respectively. A cache hit occurs in chunk 12 as shown in Fig. \ref{example_diagram}. However, if the address of the request is 0x008000001, the common-tag is matched but no chunk-tag is hit. In this case, this request misses the cache. According the replacement policy, a chunk should be considered as eviction from these 16 chunks. If the common-tag of the selected chunk is equal to that of the request, only the selected chunk is required to be evicted. Otherwise, the entire cache line that the selected chunk stays in should be evicted.

Secondly, we discuss the design of 32-bit chunk-tag depicted in Fig. \ref{chunk_tag_32bit}. For a 64GB address space, 32-bit is wide enough to index all chunks that can map to one set. Thus, we do not need a common-tag in this case. Similar to the design of 16-bit chunk-tag, we use the upper 2 bits of $Byte\_Index$ as the lower 2 bits of a 32-bit chunk-tag. And we use the 24-bit of default tag as the upper part of the chunk-tag. Therefore, actually only 26 bits are valid within the 32-bit chunk-tag since the address width we assume here is 36 bits. A 32-bit chunk-tag is stored in one bank of the shared memory. And we use the original tag array to store tags for 128 bytes data blocks. In this way, a 32 bytes data block can be placed in any of the 16 chunks of one set as long as they map to this set. For example, a request of 32 bytes needs to access address 0x008000009 and chunk 23 is hit since they have the same chunk-tag as shown in Fig. \ref{example_diagram}. If the address of the request is 0x00800000F, no chunk is matched. In this case, the potential evicted chunks can be selected from all these 16 chunks of this set. For requests of 128 bytes, tags stored in the original tag array are used to be compared and an entire cache line should be considered as eviction when a cache miss happens.



\subsection{Modification to Shared Memory}

The shared memory and L1 cache could have been configured dynamically by programmers since Fermi architecture \cite{FERMI}. We do not know the details about how this on-chip memory is configured as it has not been published by NVIDIA. In this paper, we assume that the shared memory and L1 cache share the same on-chip memory structure whose total capacity is 64KB as illustrated in Fig. \ref{shared_memory_address_mapping}. In Fig. \ref{shared_memory_address_mapping}(a), the memory is configured as a 48KB shared memory and a 16KB L1 cache. In Fig. \ref{shared_memory_address_mapping}(b), the sizes of the shared memory and L1 cache are 16KB and 48KB, respectively. Because the size of L1 cache is not fixed, we should make our approach work for both configurations.

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.49\textwidth]{figures/shared_memory_address_mapping.eps}\\
  \caption{shared memory address partition}\label{shared_memory_address_mapping}
\end{figure}

If we use 16 bits, which are half of the width of a bank of the shared memory, to represent a chunk-tag, then we need total 1KB and 3KB to store all chunk-tags for 16KB and 48KB L1 caches, respectively. In the 32-bit case, 2KB and 6KB of the shared memory are required for 16KB and 48KB caches, respectively. In this paper, we mainly discuss the design when the chunk-tag length is 32 bits.

In order to access L1 cache and chunk-tags stored in the shared memory in parallel, we need to separate 6KB memory space from the shared memory and make it work independently. To satisfy the two kinds of configurations of the shared memory and L1 cache, we choose the space of 10KB-15KB as the separate region as shown in Fig. \ref{shared_memory_address_mapping}. Consequently, this dedicated region can be used as both the shared memory and chunk-tags. We assume that the unused shared memory is always located in the upper address space and hence the separate region should be indexed by upper address space. To achieve this point, we need to remap logical addresses and physical addresses of the shared memory. TABLE \ref{mapping_table_48KB} and TABLE \ref{mapping_table_16KB} show the address mapping for the shared memory of 48KB and 16KB, respectively. For the 48KB shared memory with 32 32-bit banks, there are 48$\times$1024/(32$\times$4)=384 entries in each bank. indicating that 9 bits are sufficient to address all entries. However, because the offsets in each space are fixed, the addition (+48) and subtraction (-256) can be done by 5 bits and 1 bit, respectively.%As we see from TABLE \ref{sm16}, the logical address and physical address are equal.

\begin{table}
  \centering
  \begin{tabular}{!{\vrule width 1.5pt}c|c|c!{\vrule width 1.5pt}}
    \hlinewd{1.5pt}
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
    logical address & offset & physical address \\
    \hline
    0-79 & +0 & 0-79 \\
    \hline
    80-335 & +48 & 128-383 \\
    \hline
    336-383 & -256 & 80-127 \\
    \hline
    384-511 & +0 & 384-511 \\
    \hlinewd{1.5pt}
  \end{tabular}
  \caption{ Address mapping for 48KB shared memory and 16KB L1 cache}\label{mapping_table_48KB}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{!{\vrule width 1.5pt}c|c|c!{\vrule width 1.5pt}}
    \hlinewd{1.5pt}
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
    logical address & offset & physical address \\
    \hline
    0-79 & +0 & 0-79 \\
    \hline
    80-127 & +0 & 80-127 \\
    \hline
    128-383 & +0 & 128-383 \\
    \hlinewd{1.5pt}
  \end{tabular}
  \caption{ Address mapping for 16KB shared memory and 48KB L1 cache}\label{mapping_table_16KB}
\end{table}


\subsection{Cache Architecture}

\begin{figure*}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.9\textwidth]{figures/cache_architecture.eps}\\
  \caption{Cache architecture}\label{cache_architecture}
\end{figure*}

The cache architecture of our approach is illustrated in Fig. \ref{cache_architecture}. To obtain all the chunk-tags of one set in one cycle, bank conflicts should be avoided when accessing the shared memory. Consequently, we must guarantee that chunk-tags of one set are stored in different banks of the shared memory. When L1 cache is configured as 16KB, the associativity is 4 and there are 16 chunks in a set. Then we need 16 banks to store the 32-bit chunk-tags for fine-grain cache accesses. While the associativity becomes 6 if L1 cache is configured as 48KB, requiring 24 banks to store the chunk-tags of a set. For the case of 48KB L1 cache, a simple way to access tags stored in the shared memory in parallel is to store the chunk-tags of one set in the same address of each bank. Then the 24 banks are able to be accessed simultaneous by the same address. But obviously this method wastes the shared memory resource as the remaining 8 banks are always idle. In stead, we assign the chunk-tags of successive chunks to successive banks to fully utilize the shared memory resource. However, chunk-tags of a set may be stored in different positions among banks. For example, the first 8 chunk-tags of the second set are stored in the first entry of the last 8 banks while the last 16 chunk-tags need to be stored in the second entry of the first 16 banks. The address of each bank is computed by address decoders shown in Fig. \ref{cache_architecture}. Note that the real shared memory and L1 cache cannot be accessed simultaneously in the baseline GPU. Both shared memory instructions and global memory instructions are issued by load-store unit. In a particular cycle, only one memory instruction can be issued.

\begin{equation}\label{16kb}
  address=Set\_Index>>1
\end{equation}

\begin{equation}\label{48kb}
  address=Set\_Index-(Set\_Index+Group\_Id)>>2
\end{equation}



To reduce the number of address decoders, we divide the 32 banks into 4 groups, each of which contains 8 banks. Therefore, 2 groups are required by one set when L1 cache is 16KB and 3 groups are used when L1 cache is 48KB. The address of each bank within a group is always identical. The detail of the address decoder is shown in Fig. \ref{address_decoder}. The $Set\_Index$ is computed according to the address mapping in TABLE \ref{mapping_table_16KB} and TABLE \ref{mapping_table_48KB}, which is 6 bits at most. We use $Set\_Index$ to compute the address of each bank. Addresses of the 16KB and 48KB L1 caches are computed by Eq. \ref{16kb} and Eq. \ref{48kb}, respectively. We use the following two examples to illustrate the process of address computing for 16KB L1 cache and 48KB L1 cache, respectively. This design still works for the case of 16-bit chunk-tags although 12 banks are required when L1 cache is 48KB.


\begin{itemize}
  \item For 16KB L1 cache, Set 0 needs to access the first and second group of banks. The address of the first and second group is 0 according to Eq. \ref{16kb}. For Set 1 whose tags are stored in the third group and the fourth group, the address computed by Eq. \ref{16kb} is still 1.
  \item For 48KB L1 cache, Set 0 needs to access Group 0, Group 1 and Group 2. The addresses of these three groups are same, which is 0 according to Eq. \ref{48kb}. For Set 1, Group 3, Group 0 and Group 1 are required to access. Based on Eq. \ref{48kb}, the address of Group 3 is 0 while the address of Group 0 and Group 1 is 1.
\end{itemize}

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.3\textwidth]{figures/address_decoder.eps}\\
  \caption{Address decoder}\label{address_decoder}
\end{figure}


In our approach, we still give higher priority for the shared data that needs to be stored in the shared memory. Only particular sets are allowed to be accessed in fine grain if there is not enough shared memory that can be used for all chunk-tags. The number of these particular sets are determined by Eq. \ref{set_num} before a kernel is running. $SHM_{total}$ is the total capacity of the shared memory, which is 48KB or 16KB. $Max\_CTA$ is the maximum number of CTAs a SMP can hold for current kernel. $SHM_{CTA}$ is the size of the shared memory that a CTA required. Currently, we simply allow the first $Num\_set$ sets to be accessed in fine granularity.

\begin{equation}\label{set_num}
  Num\_set =\lfloor\frac{SHM_{total} - Max\_CTA \times SHM_{CTA}}{Tag\_Size\_Per\_Set} \rfloor
\end{equation}

%The replacing policy is LRU. When 32 bytes requests miss the cache, an alternative is searched through all the 16 32 bytes chunks based on LRU policy. If the final chunk has the same common tag with the missed request, then only this chunk is allocated. Otherwise, the other three chunks need to be invalided.




\subsection{LRU State}
The least recently used (LRU) states are maintained both for each 32-byte chunks and 128-byte cache lines. When requests of 128 bytes miss the cache, 128-byte cache lines are considered for eviction. We update the LRU state of a cache line as well when any of its chunks are accessed. Consequently, we can guarantee that the evicted 128-byte cache lines or 32-byte chunks are always the least recently used. On the other hand, if a 128- byte request access the cache, all LRU states of its 4 32-byte chunks are updated simultaneously. When a 32-byte request evicts a chunk located in a cache line that is occupied by a 128-byte block, we only need to replace one chunk and keep the other three chunks valid. The other three chunks thereby need to keep their own LRU states.

%\subsection{Write Opeartion}
%There are two types of write operations that access L1 data cache, namely global write operation and local write operation. For write miss, no cache line is allocated for this miss and the write request is simply sent to lower level memory. So nothing is taken to the cache when a write miss occurs. For global write hit, the data in the cache line which is hit is written back to lower level memory and the status of the cache line is set as INVALID. In this case, the 32 bytes chunk or 128 bytes cache line that are hit are evicted. For local write operation, the policy is write-back. So status of the chunk and its corresponding cache line are set as dirty. The diagram is shown in Figure .

\subsection{Cache Coherence}
Data fetched by requests of 32 bytes and 128 bytes is not always independent since it is likely that a 32-byte request and a 128-byte requests that have some overlaps. If the 32-byte and the 128-byte data blocks are both in L1 cache at the same time, it does not matter for read operations even some bytes are overlapped between these two data blocks. However, when a write hit operation happens, we can only write one of the copies. The other copy is still clean, making the cache incoherent. In order to prevent this issue, we need to apply the coherence protocol inside L1 cache. There are many possible policies we can select but we just apply a simple one in this paper as described below.
\begin{itemize}
  \item The 32-byte block that has been in L1 cache is invalidated if a 128-byte block possessing the same 32-byte block is incoming.
  \item If a 128-byte block is already in the cache line while a 32-byte block inside the 128-byte block is going to be written, then we do partial modification for the 128-byte block.
\end{itemize}
