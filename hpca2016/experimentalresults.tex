\section{Experimental results}
\label{result}
In this section, we analyze the experimental results in detail in terms of performance, L1 cache miss rate, cache efficiency and overhead.

\subsection{Performance}
\label{performance}

For comparison, we implement the following 4 approaches.
\begin{itemize}
  \item Without considering the overhead, each chunk is associated with a 16-bit chunk-tag which is implemented by the dedicated tag array and a common-tag is bound to each cache line. We name this approach 16-BIT.
  \item Without considering the overhead, each chunk is associated with a 32-bit chunk-tag which is implemented by the dedicated tag array. We name this approach 32-BIT.
  \item Each chunk is associated with a 16-bit chunk-tag which is implemented by the unused shared memory and a common-tag is bound to each cache line. We name this approach 16-BIT-SHM.
  \item Each chunk is associated with a 32-bit chunk-tag which is implemented by the unused shared memory. We name this approach 32-BIT-SHM.
\end{itemize}

In workloads that utilize the shared memory, the required size of the shared memory of each CTA in a kernel does not vary with the capacity of the shared memory, but it can affect the number of CTAs allocated to each SMP if the shared memory becomes the key constrained resource, which eventually affect the size of the unused shared memory and the performance in our mechanism. Therefore, we compare the performance for two cache configurations, 16KB L1 cache (48KB shared memory) and 48KB L1 cache (16KB shared memory).

\subsubsection{\textbf{16KB L1 cache and 48KB shared memory}}

The performance is represented by instructions per cycle (IPC). We normalize the IPCs to that of the baseline GPU. As shown in Fig. \ref{small_cacheline}, we firstly compare the results when applying cache lines of 32 bytes (32B-CL) and 64 bytes (64B-CL) to the results of the baseline GPU.

\begin{figure} \centering
\subfloat[Irregular applications] { \label{small_cacheline_a}
\includegraphics[width=0.49\textwidth]{figures/small_request_ia.eps}
}
\\
\subfloat[Regular applications] { \label{small_cacheline_b}
\includegraphics[width=0.49\textwidth]{figures/small_request_ra.eps}
}
\caption{Normalized IPCs when using cache lines of 32 bytes and 64 bytes}
\label{small_cacheline}
\end{figure}

%\begin{figure}
%  \centering
%  % Requires \usepackage{graphicx}
%  \includegraphics[width=0.49\textwidth]{small_cacheline.eps}\\
%  \caption{small cacheline}\label{small_cacheline}
%\end{figure}

Simply reducing the size of cache lines to 32 bytes or 64 bytes can also provide benefits for irregular applications since most of requests in these applications are 32 bytes. However, as we have mentioned in Section \ref{motivation}, the drawback of this approach is that more requests are generated than the request number in the baseline GPU, which may increase the traffic in the memory system. In the mechanism of 32B-CL, another downside is that it completely breaks the spatial locality of requests. Therefore, some regular applications suffer from the degradation of IPCs when the cache line is 32 bytes as shown in Fig. \ref{small_cacheline_b}.

Overall, the average improvements of 64B-CL and 32B-CL approaches for irregular applications are 26\% and 34\%, respectively. However, for regular applications, the normalized IPCs of 64B-CL and 32B-CL approaches are only 96\% and 80\%, respectively. Due to the drawbacks of these two mechanisms, it is obvious that we still have space to explore the potential to increase the IPCs for irregular applications without degrading the IPCs for regular applications.

We show the evaluations of our mechanism in Fig. \ref{ipc_16KB_cache}. As shown, the IPCs of 16-BIT-SHM and 32-BIT-SHM are similar to those of 16-BIT and 32-BIT. The reason is that most of the shared memory are unused when its capacity is 48KB as shown in Fig. \ref{rr_and_usm}. The size of the unused shared memory is enough to store all chunk-tags for fine-grain cache management. Note that, in the approach of 16-BIT-SHM, we need a common-tag for each cache line. In theory, this approach is not as flexible as the approach of 32-BIT-SHM. The similar results of this two approaches indicate that 16 bits are sufficient to index most of addresses for these applications. The IPCs for regular applications do not change very much when applying Amoeba-cache and our mechanism as shown in Fig. \ref{ra_16KB}, except SSSP. We recover the degradation for SRAD, BP, PF, BT, DWT, CCL and GCU in Fig. \ref{small_cacheline_b}. The reason is that we keep the spatial locality for 128 bytes requests without increasing the number of traffic in the memory system. For SSSP, 71\% of its requests are 32 bytes and the cache efficiency is 53\%, we thereby consider this applications as a ``pseudo regular application". That explains why it can benefit from the approaches of fine-grain cache management, including Amoeba-cache and our mechanisms. %Amoeba cache performs better than us in SRAD2, that is because most of the requests in SRAD2 are 64 bytes and amoeba cache supports the access in 64 bytes. But, we regard 64 bytes as 128 bytes in our scheme.

Overall, the average improvement of 32-BIT-SHM and 16-BIT-SHM for irregular applications is 53\%. For Amoeba-cache, the speed up is 41\% on average.

\begin{figure} \centering
\subfloat[Irregular applications] { \label{ia_16KB}
\includegraphics[width=0.49\textwidth]{figures/ia_16KB.eps}
}\\
\subfloat[Regular applications] { \label{ra_16KB}
\includegraphics[width=0.49\textwidth]{figures/ra_16KB.eps}
}
\caption{Comparisons of Normalized IPCs when L1 cache is 16KB}
\label{ipc_16KB_cache}
\end{figure}

\subsubsection{\textbf{48KB L1 cache and 16KB shared memory}}
The corresponding evaluations when L1 cache is configured as 48KB are shown in Fig. \ref{ipc_48KB_cache}. Even for irregular applications, our approach can not bring as many benefits as that in the case where the cache size is 16KB. The reason is easy to understand. When the shared memory is 16KB, the size of the unused shared memory is fewer compared to that in the 48KB shared memory, which effects the number of chunk-tags that can be stored in the share memory. On the other hand, more space is required to store chunk-tags for the 48KB cache than for the 16KB cache. Totally, we need 6KB shared memory while the size is only 2KB as we mentioned in Section \ref{implementation}. Let us take NW in which the IPC is decreased by 6.5\% comparing to the mechanism of 32-BIT or 16-BIT as an example. The CTAs that are permitted to allocate on each SMP is restricted by the maximum CTAs that a SMP can hold which is 8 in Fermi with the 16KB L1 cache. However, the shared memory becomes the bottleneck that limits the number of CTAs when L1 cache is 48KB and only 7 CTAs are allowed to run in a SMP simultaneously. Each CTA requires 2180 bytes shared memory and only 11 sets of L1 cache can be accessed in fine granularity with 32-BIT-SHM while the total set number is 64 in NW according to Eq. \ref{set_num}. In addition, another important reason that we can not ignore is as the cache size is increased from 16KB to 48KB, more resources are available, resulting in lower cache miss rate. For instance, the cache miss rate is only 8.6\% in SM when the cache is 48KB. As a general rule, fine granularity cache management has fewer benefits on larger cache.

In a word, the approaches of 32-BIT-SHM and 16-BIT-SHM can increase the performance by 21\% on average for irregular applications. The average improvements of Amoeba-cache is 15\%.

\begin{figure} \centering
\subfloat[Irregular applications] { \label{ia_48KB}
\includegraphics[width=0.49\textwidth]{figures/ia_48KB.eps}
}\\
\subfloat[Regular applications] { \label{RA_48KB}
\includegraphics[width=0.49\textwidth]{figures/ra_48KB.eps}
}
\caption{Comparisons of Normalized IPCs when L1 cache is 48KB}
\label{ipc_48KB_cache}
\end{figure}

\subsection{L1 Cache Miss Rate}

The miss rate when L1 cache is configured as 16KB is shown in Fig. \ref{cache_miss_rate}. Fine-grain management of cache lines can result in lower miss rate when spatial locality is not good among requests. In Fig. \ref{cache_miss_rate}, we can observe that the miss rate is degraded significantly in benchmarks PARTF, SPMV, BH, PVA, II, SM and GCO. As more space can be provided for requests of 32 bytes by our apprpach, less evictions occur in L1 cache. That is why the miss rate is decreased even for BH in which only one request is generated per load or store instruction shown in Fig. \ref{ce_and_ar} of Section \ref{motivation}.

On the other hand, the miss rate is increased if the spatial locality can not be explored by small cache lines. For example, SP suffers from a higher miss rate when using our approach. But as shown in Fig. \ref{ipc_16KB_cache}, the IPC of SP is still improved by more than 2 times. We find that the benefits result from the reduced amount of data transferred between L1 and L2 cache since only 32 bytes are required to be fetched from L2 cache when a request of 32 bytes has a cache miss instead of a cache line which is 128 bytes. To verify this point, we modify the baseline architecture and set the fetch size as 32 bytes for a cache miss in the simulator. Eventually, the result shows a improvement of more than 2 times, which is similar to that of our apprpach.

\begin{figure*}[!tb]
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.99\textwidth]{figures/cache_miss_rate.eps}\\
  \caption{Cache miss rate}\label{cache_miss_rate}
\end{figure*}

\subsection{Cache Efficiency}
We have shown the cache efficiency for the baseline GPU in Section \ref{motivation}. For comparison, we compute the cache efficiency for Amoeba-cache and our approach as well. As shown in Fig. \ref{cache_efficiency_comparison}, the cache efficiencies of most benchmarks are improved except BP and PF by applying our approach comparing to the cache efficiencies in the baseline GPU. Because we measure the cache efficiency in 32-byte grain, the cache efficiency ought to be 100\% when a 32-byte block is evicted. For BP and PF, they have higher cache miss rate when applying our approach as shown in Fig. \ref{cache_miss_rate} and the minority of requests are 32 bytes in these two applications. Higher miss rate means more evictions. When most of the evicted data is 128 bytes, cache efficiency is degraded as well.

Furthermore, Amoeba-cache performs better than our approach especially in SM, LUD, HS and APSP. For LUD, HS and APSP, 64-byte requests take a big ratio as shown in Fig. \ref{rr_and_usm}. And Amoeba-cache supports the granularity of 64-byte cache management while we treat 64-byte requests the same as 128-byte requests in our approach. Thereby the cache efficiencies of our approach are not as good as those of Amoeba-cache. However, this reason cannot explain the degradation of cache efficiency for SM in which only 4.7\% of the requests are 64 bytes. We note that our approach has a better miss rate than Amoeba-cache, meaning that Amoeba-cache results in more evictions than our approach. What's more, our approach induces more evictions of 128-byte blocks than Amoeba-cache, which is about 1.36 times. In other words, more evictions of 32-byte chunks, which indicates that the cache efficiency of these evicted cache block is 100\%, occur in Amoeba-cache. Consequently, the total cache efficiency in Amoeba-cache is higher than that in our approach for SM.

\begin{figure*}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.99\textwidth]{figures/cache_efficiency.eps}\\
  \caption{Cache efficiency comparison}\label{cache_efficiency_comparison}
\end{figure*}

\subsection{Overheand}

\subsubsection{\textbf{Hardware Overhead}}

Since the extra chunk-tags are stored in the shared memory, we just need to add some logical circuits to implement our mechanism. In fine-grain cache management, a chunk needs 2-bit status and 5-bit LRU counter. Totally, 1.3KB storage is required when the capacity of L1 cache is 48KB. In addition, as we can see in Fig. \ref{cache_architecture}, we need 4 address decoders, 32 comparators and a 5-bit adder which is used for address mapping of the shared memory in each SMP. Compared with the size of data array, the area of these circuits are negligible.

\subsubsection{\textbf{Power and Energy}}

The shared memory should be always switched on during the execution of kernels no matter how much data is stored in it. Thus there is always leakage power in the shared memory. As we utilize the shared memory to store chunk-tags, we do not induce extra leakage power in our approach. And we do not need to change the structure of the data array. Therefore, the main overhead of our approach is the dynamic energy when accessing the chunk-tags stored in the shared memory. To access these chunk-tags and the data array concurrently, we separate a particular region with the size of 6KB from the shared memory, which also results in the reduction of dynamic energy of accessing chunk-tags since the capacity is shrunk. We use CACTI \cite{CACTI} to estimate the energy consumption of this chunk-tag array. The energy per access of the tag array of the conventional L1 cache is 0.00078nj, while the energy per access of the chunk-tag array in the shared memory of our approach is 0.00235nj. But the power consumption of L1 cache is usually dominated by the data array since the size of the data array is much larger than the tag array. Even the dynamic energy of accessing chunk-tags in the shared memory is about 3 times over that in conventional L1 cache, but it is still negligible when comparing to the dynamic energy of the data array which is 0.0483nj per access (4.9\%). %What's more, the total power consumption of L1 cache in baseline architecture is only b\% of the total power. So the power variation is negligible at the GPU level.

\subsection{Utilizing the unused shared memory as L1 cache}
We utilize the unused shared memory to store chunk-tags. But the maximum usage for chunk-tags is only 2KB and 6KB for the 16KB and 48KB L1 cache, respectively. A fraction of the shared memory is still idle. To make this unused shared memory work, we further utilize it as the data array of L1 cache. To access chunk-tags and data in parallel, we need to separate a dedicated region to store chunk-tags. The size of this region is determined by Eq. \ref{cache_size} when all on-chip memory are considered as L1 cache. The total capacity of the shared memory and L1 cache is 64KB.
\begin{equation}\label{cache_size}
  (Cache\_Size/128B)\times4\times4B+Cache\_Size=64\times1024B
\end{equation}

In Eq. \ref{cache_size}, 128B is the cache line size, 4 means 4 32-byte chunks in each cache line. 4B is the size of each chunk-tag (32 bits). Then $Cache\_Size$ is equal to 56.89KB, implying the maximum cache size is about 56KB.  The remaining 8KB is used to store chunk-tags (In fact, 7KB is sufficient). For a 56KB cache, it can be configured as a 7-way set associativity cache with 64 sets.
We should also note that different usage of the shared memory can result in different configurations of L1 cache. In this paper, we make 4 categories of configurations for L1 cache according to the usage of the shared memory. And we allow half of the cache to be accessed in fine granularity at least. The 4 configurations are listed in TABLE \ref{dynamic_cache}.

\begin{table}
  \centering
  \caption{Dynamic cache configurations}\label{dynamic_cache}
\begin{tabular}{!{\vrule width 1.5pt}c|c|c!{\vrule width 1.5pt}}
  \hlinewd{1.5pt}
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  Cache Capacity & Set & Associativity \\
  \hline
  56KB & 64 & 7 \\
  \hline
  48KB & 64 & 6 \\
  \hline
  32KB & 64 & 4 \\
  \hline
  16KB & 32 & 4 \\
  \hlinewd{1.5pt}
\end{tabular}
\end{table}

When the shared memory is 48KB, the cache size is determined by the following mechanism.
\begin{itemize}
  \item unused shared memory $\geq$ 44KB, Cache\_Size = 56KB.
  \item 35KB $\leq$ unused shared memory $<$ 44KB, Cache\_Size = 48KB.
  \item 18KB $\leq$ unused shared memory $<$ 35KB, Cache\_Size = 32KB.
  \item unused shared memory $<$ 18KB, Cache\_Size = 16KB.
\end{itemize}

When the shared memory is 16KB, the cache size is determined by the following mechanism
\begin{itemize}
  \item unused shared memory $\geq$ 12KB, Cache\_Size = 56KB.
  \item unused shared memory $<$ 12KB, Cache\_Size = 48KB.
\end{itemize}



\begin{figure*}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.99\textwidth]{figures/shared_memory_dynamic.eps}\\
  \caption{Normalized IPC of dynamic L1 cache with 16KB shared memory}\label{shared_memory_dynamic}
\end{figure*}

\begin{figure*}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.99\textwidth]{figures/shared_memory_dynamic_48KB.eps}\\
  \caption{Normalized IPC of dynamic L1 cache with 16KB shared memory}\label{shared_memory_dynamic_48KB}
\end{figure*}

To work with this dynamic cache mechanism, the structure of the shared memory needs to be reconsidered, including the address decodes, address mapping and the grouping of the shared memory banks. But we do not discuss it in this paper. The normalized IPCs of this dynamic cache mechanism for the 48KB shared memory and 16KB shared memory are shown in Fig. \ref{shared_memory_dynamic} and Fig. \ref{shared_memory_dynamic_48KB}, respectively. When the shared memory is 48KB, the average improvement is 38\%, which is 11\% higher than the average improvement of 32-BIT-SHM comparing to the baseline GPU. For the case of the 16KB shared memory, since only 8KB can be used as extra data array at most, the IPCs are increased by 13\%, which is similar to the 12\% improvement obtained from 32-BIT-SHM.
